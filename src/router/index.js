import Vue from 'vue'
import VueRouter from 'vue-router'

import Home from '../views/Home.vue'
import Team from '../views/Team.vue'
import ShowTeam from '../views/ShowTeam.vue'
import Contact from '../views/Contact.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/team',
    name: 'Team',
    component: Team
  },
  {
    path: '/team/:teamId',
    name: 'showTeam',
    component: ShowTeam
  },
  {
    path: '/contact',
    name: 'Contact',
    component: Contact
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
