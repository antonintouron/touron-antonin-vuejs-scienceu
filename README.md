# Dev

* `https://vuejs-app-esgi-dev.herokuapp.com/`

# Staging

* `https://vuejs-app-esgi-staging.herokuapp.com/`

# Prod

* `https://vuejs-app-esgi.herokuapp.com/`

### Sonar Cloud

* https://sonarcloud.io/organizations/antonintouron-vuejs/projects

### Build & Run Image Docker

* Build => `docker build -t docker-vuejs-demo .`
* Run => `docker run -it -p 8080:8080 --rm --name dockerize-vuejs-app-1
   docker-vuejs-demo`

## Docker Gitlab Registry

* Login to gitlab registry `docker login registry.gitlab.com` (use deploy token username and password)
